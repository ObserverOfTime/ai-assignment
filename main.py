#!/usr/bin/env python3

from sys import argv, stderr, stdin
from typing import List

import ai


def main(args: List[str]) -> None:
    """Run the game."""
    argc = len(args)
    if argc and args[0] == 'minmax':
        if argc > 1:
            bot = ai.MinMaxBot(depth=int(args[1]))
        else:
            bot = ai.MinMaxBot()
    else:
        bot = ai.RandomBot()
    while not stdin.closed:
        try:
            line = stdin.readline()
            if len(line) == 0:
                break
            tokens = line.strip().split()
            if len(tokens) == 0:
                continue
            cmd = tokens[0]
            if cmd == 'Output':
                continue
            bot.run(cmd, line)
        except EOFError:
            break


if __name__ == '__main__':
    main(argv[1:])
