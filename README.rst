Artificial Intelligence Assignment
==================================

Install `pipenv <https://docs.pipenv.org/en/latest/install/>`_

Clone the project:

.. sourcecode:: sh

   git clone https://gitlab.com/ObserverOfTime/ai-assignment
   cd ai-assignment

Install the requirements:

.. sourcecode:: sh

   pipenv update --dev

Run ``flake8`` to check the coding style:

.. sourcecode:: sh

   pipenv run flake8 ai/

Download match-wrapper_ & connect-four-engine_.

Run the bot:

.. sourcecode:: sh

   java -jar match-wrapper-1.4.1.jar "$(<wrapper-commands.json)"

.. _match-wrapper: https://github.com/riddlesio/match-wrapper/releases/tag/1.4.1
.. _connect-four-engine: https://github.com/riddlesio/connect-four-engine/releases/tag/1.0.1
