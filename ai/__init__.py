"""Artificial Intelligence Assignment."""
from .bot import RandomBot
from .minmax import MinMaxBot

__all__ = ['RandomBot', 'MinMaxBot']
