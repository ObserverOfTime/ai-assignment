from abc import ABCMeta, abstractmethod
from functools import lru_cache
from random import randrange
from typing import List, Optional

from . import engine as e


class UnrecognisedCommand(Exception):
    """Exception raised when a command is not recognised."""

    def __str__(self) -> str:
        """Return the exception message."""
        return '{0} command "{1}" not recognised'.format(*self.args)


class BaseBot(metaclass=ABCMeta):
    """
    Abstract bot template.

    :ivar settings: The settings object of your bot.
    :type settings: Settings
    :ivar round: The current round of the game.
    :type round: int
    :ivar opponent: The id of your bot's opponent.
    :type opponent: int
    :ivar field: The playing field of the game.
    :type field: Optional[e.Field]
    """

    def __init__(self) -> None:
        self.round: int = 0
        self.opponent: int = -1
        self.field: Optional[e.Field] = None
        self.settings: e.Settings = e.Settings()

    @abstractmethod
    def make_move(self, timeout: int) -> None:
        """
        Calculate and execute the next move.

        Make the move by calling :func:`engine.place_disc` exactly once.

        :param timeout: The time your bot has to respond in milliseconds.
        :type timeout: int
        """
        pass

    @property
    @lru_cache()
    def id(self) -> int:
        """Get the id of your bot."""
        return self.settings.your_botid

    @property
    @lru_cache()
    def rows(self) -> int:
        """Get the number of field rows."""
        return self.settings.field_height

    @property
    @lru_cache()
    def cols(self) -> int:
        """Get the number of field columns."""
        return self.settings.field_width

    def run(self, cmd: str, input: str) -> None:
        """
        Run the given command.

        :param cmd: The command to run.
        :type cmd: str
        :param input: The input given to your bot.
        :type input: str
        """
        if hasattr(self, '_run_' + cmd):
            getattr(self, '_run_' + cmd)(input)
        else:
            raise UnrecognisedCommand('???', input)

    def _run_settings(self, input: str) -> None:
        """
        Handle input intended to set the settings of your bot.

        :param input: The input given to your bot.
        :type input: str
        """
        tokens: List[str] = input.strip().split()[1:]
        cmd: str = tokens[0]
        if cmd in ('timebank', 'time_per_move', 'field_height',
                   'field_width', 'your_botid'):
            self.settings[cmd] = int(tokens[1])
        elif cmd == 'your_bot':
            self.settings[cmd] = tokens[1]
        elif cmd == 'player_names':
            self.settings[cmd] = tokens[1:]
            self.opponent = int(not self.id)
        else:
            raise UnrecognisedCommand('Settings', input)

    def _run_update(self, input: str) -> None:
        """
        Handle input intended to update the game.

        :param input: The input given to your bot.
        :type input: str
        """
        tokens: List[str] = input.strip().split()[2:]
        cmd: str = tokens[0]
        if cmd == 'round':
            self.round = int(tokens[-1])
        elif cmd == 'field':
            self.field = e.Field(tokens[-1], (self.rows, self.cols))
        else:
            raise UnrecognisedCommand('Update', input)

    def _run_action(self, input: str) -> None:
        """
        Handle input intended to prompt your bot to take an action.

        :param input: The input given to your bot.
        :type input: str
        """
        tokens: List[str] = input.strip().split()[1:]
        if tokens[0] == 'move':
            self.make_move(int(tokens[1]))
        else:
            raise UnrecognisedCommand('Action', input)


class RandomBot(BaseBot):
    """Always make a move at random."""

    def make_move(self, timeout: int) -> None:
        """Place a disk at random."""
        e.place_disc(randrange(self.cols))


__all__ = ['BaseBot', 'RandomBot']
