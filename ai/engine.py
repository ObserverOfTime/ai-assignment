from sys import stderr, stdout
from typing import Any, Tuple

import numpy as np


class Settings:
    """
    Contains a game setting.

    :ivar timebank: Maximum time in milliseconds that
                    your bot can have in its time bank.
    :type timebank: int
    :ivar time_per_move: Time in milliseconds that is added
                         to your bot's time bank each move.
    :type time_per_move: int
    :ivar player_names: A list of all player names in this
                        match, including your bot's name.
    :type player_names: Tuple[str, str]
    :ivar your_bot: The name of your bot for this match.
    :type your_bot: str
    :ivar your_botid: The ID number of your bot as in the field
                      updates (same number as in player name).
    :type your_botid: int
    :ivar field_width: The number of columns on the board.
    :type field_width: int
    :ivar field_height: The number of rows on the board.
    :type field_height: int
    """

    def __init__(self) -> None:
        self.timebank: int = -1
        self.time_per_move: int = -1
        self.player_names: Tuple[str, str] = ('', '')
        self.your_bot: str = ''
        self.your_botid: int = -1
        self.field_width: int = -1
        self.field_height: int = -1

    def __getitem__(self, item: str) -> Any:
        """Get settings item by name."""
        return getattr(self, item)

    def __setitem__(self, item: str, value: Any) -> None:
        """Set settings item by name."""
        setattr(self, item, value)


class Field(np.ndarray):
    """Represents the playing field of the game."""

    def __new__(cls, input: str, shape: Tuple[int, int]):
        """
        Create a new Field instance.

        :param input: An input string representing the field.
        :type input: str
        :param shape: The shape (rows, columns) of the field.
        :type shape: Tuple[int, int]
        """
        return np.reshape(input.split(','), shape).view(cls)

    def __str__(self) -> str:
        """Return the field as a string."""
        return ','.join(self.flatten().astype(str))


def place_disc(column: int) -> None:
    """
    Print the next move.

    :param column: The column your bot will place a disc in,
                   where the leftmost column is ``0`` and the
                   rightmost column is ``field_width - 1``.
    :type column: int
    """
    stdout.write('place_disc %d\n' % column)
    stdout.flush()


def log(msg: Any) -> None:
    """Log a message to ``stderr``."""
    print(msg, file=stderr)


__all__ = ['Settings', 'Field', 'place_disc', 'log']
