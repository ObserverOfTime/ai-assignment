from functools import lru_cache
from itertools import groupby
from random import choice
from typing import List, Tuple

import numpy as np

from . import engine as e
from .bot import BaseBot

Move = Tuple[str, int, int]
Scores = List[int]


class MinMaxBot(BaseBot):
    """
    Bot which implements a minmax algorithm.

    :ivar depth: The depth of the algorith.
    :type depth: int
    """

    def __init__(self, depth: int = 4):
        super(MinMaxBot, self).__init__()
        self.depth = depth
        self._weights = (1, 100, 1000, 100000)
        self._combo_win = str(self.id) * 4
        self._combo_lose = str(self.opponent) * 4
        self._bad_pos = (
            '.', str(self.opponent),
            '.', str(self.opponent), '.'
        )

    def best_move(self) -> int:
        """Get the best move as a column number."""
        alpha = -np.inf
        beta = np.inf
        best_moves = []
        state = self.field[::-1].T
        scores = {}

        # check bad pos .1.1. in first line
        for i in range(self.cols):
            slice = tuple(self.field[-1, i - 2:i + 3])
            if i > 1 and self._bad_pos == slice:
                return i
        for move in self._legal_moves(state, self.id):
            scores[move[1]] = self.minmax(
                self.depth - 1, move, self.id, alpha, beta, []
            )[0]
        for move in sorted(scores, key=scores.get):
            if (not best_moves or scores[best_moves[0]] -
                    scores[move] <= self._weights[1]):
                best_moves.append(move)
        return choice(best_moves)

    def minmax(self, depth: int, move: Move, bot_id: int, alpha: float,
               beta: float, states: e.Field) -> Tuple[int, e.Field]:
        """Search the tree and return the alpha value."""
        state = move[0]
        positions = move[1:]
        # return score in last recursion
        if depth == 0:
            return self._score_total(state), state

        # return score * depth if gameover
        gameover = self._gameover(move)
        if gameover:
            return gameover * depth, state

        # determine opponent and set default alpha
        opp = str(self.opponent if bot_id == self.id else self.id)
        best_score = np.inf if bot_id == self.id else -np.inf

        # get legal moves from this state
        legal_moves = self._legal_moves(state, opp)
        if len(legal_moves) == 0:
            return self._score_total(state, positions), state

        # minmax recursive
        for move in legal_moves:
            score, future_states = self.minmax(
                depth - 1, move, opp, alpha, beta, states
            )
            if opp == self.id:
                if score > best_score:
                    best_score = score
                    future_states = np.append(state, future_states)
                alpha = max(alpha, best_score)
            else:
                if score < best_score:
                    best_score = score
                    future_states = np.append(state, future_states)
                beta = min(beta, best_score)
            # alpha-beta pruning
            if beta < alpha:
                break
        return best_score, future_states

    @property
    @lru_cache()
    def _best_enumerate(self) -> List[int]:
        b = []
        mid = self.cols >> 1
        for i in range(mid + 1):
            b += [mid - i, mid + i]
        b.pop(0)
        if self.cols % 2 == 0:
            b.pop()
        return b

    def _legal_moves(self, state: e.Field, bot_id: int) -> Move:
        legal_moves = []
        for x in self._best_enumerate:
            if state[x, -1] != '.':
                continue
            y = np.where(state[x] == '.')[0][0]
            move = state.copy()
            move[x, y] = str(bot_id)
            legal_moves.append((move, x, y))
        return legal_moves

    def _gameover(self, move: Move) -> int:
        x = move[1]
        y = move[2]
        score = self._weights[3]
        win = self._combo_win
        lose = self._combo_lose

        # check vertical
        vertical = ''.join(move[0][x, max(y - 3, 0):y + 1])
        if win == vertical:
            return score
        elif lose == vertical:
            return -score

        # check horizontal
        horizontal = ''.join([
            move[0][i, y] for i in
            range(max(x - 3, 0), min(self.cols, x + 4))
        ])
        if win in horizontal:
            return score
        elif lose in horizontal:
            return -score

        # check diagonals
        gdiagonal = ''
        bdiagonal = ''
        for i in range(max(x - 3, 0), min(self.cols, x + 4)):
            j = x + y - i
            if 0 <= j < self.rows:
                gdiagonal += move[0][i, j]
            j = y - x + i
            if 0 <= j < self.rows:
                bdiagonal += move[0][i, j]
        if win in gdiagonal or win in bdiagonal:
            return score
        elif lose in gdiagonal or lose in bdiagonal:
            return -score

        return 0

    def _score_columns(self, state: e.Field) -> int:
        your_scores = [0, 0, 0, 0, 0]
        opp_scores = [0, 0, 0, 0, 0]

        # iterate over columns in state
        for column in state.T.tolist():
            found = [False, False]
            for item, group in groupby(column[::-1]):
                # break if score exists
                if item != '.' and any(found):
                    break
                count = min(4, len(list(group)))
                if item == str(self.id):
                    found[0] = True
                    your_scores[count] += 1
                elif item == str(self.opponent):
                    found[1] = True
                    opp_scores[count] += 1
            # return max score if won
            if your_scores[4] > 0:
                return self._weights[3]
            elif opp_scores[4] > 0:
                return -self._weights[3]

        return self._heuristic(your_scores, opp_scores)

    def _score_rows(self, state: e.Field) -> int:
        your_scores = [0, 0, 0, 0, 0]
        opp_scores = [0, 0, 0, 0, 0]

        # iterate over rows in state
        for row in state:
            row_your_scores = self._score_row(row, self.id)
            # return max score if won
            if row_your_scores[4] > 0:
                return self._weights[3]
            row_opp_scores = self._score_row(row, self.opponent)
            # return min score if lost
            if row_opp_scores[4] > 0:
                return -self._weights[3]
            # double the sum of all scores
            your_scores = [
                x + y * 2 for x, y in
                zip(your_scores, row_your_scores)
            ]
            opp_scores = [
                x + y * 2 for x, y in
                zip(opp_scores, row_opp_scores)
            ]

        return self._heuristic(your_scores, opp_scores)

    def _score_diagonals(self, state: e.Field) -> int:
        your_scores = [0, 0, 0, 0, 0]
        opp_scores = [0, 0, 0, 0, 0]

        # get all diagonals
        min_bdiag = -self.cols + 1
        fdiagonals = [[] for i in range(self.cols + self.rows - 1)]
        bdiagonals = [[] for i in range(len(fdiagonals))]
        for x in range(self.cols):
            for y in range(self.rows):
                fdiagonals[x + y].append(state[x, y])
                bdiagonals[y - x - min_bdiag].append(state[x, y])

        # enumerate all diagonal rows
        for row in fdiagonals + bdiagonals:
            if len(row) < 4:
                continue
            row_your_scores = self._score_row(row, self.id)
            # return max score if won
            if row_your_scores[4] > 0:
                return self._weights[3]
            row_opp_scores = self._score_row(row, self.opponent)
            # return min score if lost
            if row_opp_scores[4] > 0:
                return -self._weights[3]
            # triple the sum of all scores
            your_scores = [
                x + y * 3 for x, y in
                zip(your_scores, row_your_scores)
            ]
            opp_scores = [
                x + y * 3 for x, y in
                zip(opp_scores, row_opp_scores)
            ]

        return self._heuristic(your_scores, opp_scores)

    def _score_row(self, row: e.Field, bot_id: int) -> Scores:
        scores = [0, 0, 0, 0, 0]
        score = 0
        opp = str(self.opponent if bot_id == self.id else self.id)

        # enumerate all combinations in the row
        for i in range(len(row) - 3):
            combination = row[i:i + 4]
            if opp in combination:
                if score > 0:
                    scores[score] += 1
                score = 0
                continue
            count = np.count_nonzero(combination == str(bot_id))
            if count >= 4:
                return [0, 0, 0, 0, 1]
            score = max(score, count)
            # last combination
            if i == len(row) - 4 and score > 0:
                scores[score] += 1

        return scores

    def _score_total(self, state: e.Field) -> int:
        v = self._score_columns(state)
        if abs(v) >= self._weights[3]:
            return v
        h = self._score_rows(state)
        if abs(h) >= self._weights[3]:
            return h
        d = self._score_diagonals(state)
        if abs(d) >= self._weights[3]:
            return d
        return v + h + d

    def _heuristic(self, your_scores: Scores, opp_scores: Scores) -> int:
        # if won/lost
        if your_scores[4] > 0:
            return self._weights[3]
        elif opp_scores[4] > 0:
            return -self._weights[3]
        # sum score * weight
        return (
            your_scores[3] * self._weights[2] -
            opp_scores[3] * self._weights[2] +
            your_scores[2] * self._weights[1] -
            opp_scores[2] * self._weights[1]
        )

    def make_move(self, timeout: int) -> None:
        """
        Make the best move each round.

        In the first round, cover the middle.
        """
        if self.round == 1 and self.id == 0:
            e.place_disc(self.cols >> 1)
        else:
            e.place_disc(self.best_move())


__all__ = ['MinMaxBot']
